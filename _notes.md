## CAH General Notes

User Signup, allow users to select a nickname.

Everyone has ten white cards

Black card is played

Everyone responds with one white card.

Vote or Czar based selection of the best card for points

Discard selected cards.

Black card (or a point) goes to the winning card,

### Specifications

#### Black Cards

- Can specify pick two, in which case two cards are selected and their order retained when passed to the Czar.


#### White Cards

- Contain simple phrases, not a lot of data.


## Pre-Game-Flow
Using dumbguy words...

When you load up the page nothing happens. Upon selecting a nick and hitting go you are websocket connected.
It is possible to embed lobby information in a link, such that a person can follow a link to a lobby, choose their nick, and then be connected to the server.

Users generally start in the lobby, the lobby reports active servers to the players and that's about it.

When you click join game it sends a little request to join up, this may include a password, the server reports to the client that it takes a password so the user can provide that on their first attempt.

That request puts the player in the proper socket room where they will begin receiving the chat broadcasts.

The client after receiving a connection OK style response will ditch the current view and load up the actual game view

By choosing a lobby or entering a game name or hitting a join link for a game 